# set and install packages
FROM ruby:2.7-buster
RUN mkdir /garden-helper
WORKDIR /garden-helper
COPY build.sh /garden-helper/build.sh
RUN chmod +x /garden-helper/build.sh
RUN /garden-helper/build.sh $RAILS_ENVIRONMENT
COPY Gemfile /garden-helper/Gemfile
RUN touch /garden-helper/Gemfile.lock
RUN bundle install
COPY . /garden-helper
RUN yarn install --check-files

# Add a script to be executed every time the container starts.
COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 3000