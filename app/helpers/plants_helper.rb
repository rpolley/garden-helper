module PlantsHelper
  def display(value, type:)
    case type
    when :nilable
      display_nilable(value)
    when :range
      display_range(value)
    when :array
      display_array(value)
    when :boolean
      display_boolean(value)
    end
  end

  def display_nilable(value)
    value || 'unknown'
  end

  def display_boolean(value)
    value ? 'yes' : 'no'
  end

  def display_array(value)
    if value.nil? || value == []
      nil_placeholder
    else
      render 'shared/array', array: value
    end
  end

  def display_range(value)
    (lower, upper) = value
    if lower.nil? && upper.nil?
      nil_placeholder
    elsif lower.nil?
      "less than #{upper}"
    elsif upper.nil?
      "more than #{lower}"
    else
      "#{lower} - #{upper}"
    end
  end

  def preview_card(plant)
    image_url = plant.image_url || 'plant_not_found.png'
    title = plant.common_name
    details_text = 'Details'
    details_url = plants_path(plant)
    render 'shared/card', image_url: image_url, title: title, details_text: details_text, details_url: details_url
  end
end
