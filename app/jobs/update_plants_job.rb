require 'rest-client'
require 'json'
class UpdatePlantsJob < ApplicationJob
  queue_as :trefle_etl

  def perform(*_args)
    # Do something later
    logger.debug 'Starting UpdatePlantsJob'
    @refresh_cutoff = 1.week.ago
    @etl_version = 1
    @logger = Rails.logger
    @api_context = PlantApiContext.new
    @api_objs = []
    begin
      plants = edible_plants
      plants.each do |page|
        page.each do |plant_item|
          sync(plant_item)
        end
      end
    rescue RestClient::RequestFailed => e
      logger.error "request failed, caught message #{e}"
    end
  end

  def munge(item)
    specs = item['specifications']
    growth = item['growth']
    logger.debug "growth characteristics: #{growth}"

    {
      slug: item['slug'],
      common_name: item['common_name'],
      image_url: item['image_url'],
      average_height: specs['average_height']['cm'],
      nitrogen_fixation: !specs['nitrogen_fixation'].nil?,
      days_to_harvest: growth['days_to_harvest'],
      row_spacing: growth['row_spacing']['cm'],
      ph_maximum: growth['ph_maximum'],
      ph_minimum: growth['ph_minimum'],
      prefered_light: growth['light'],
      prefered_atmospheric_humidity: growth['atmospheric_humidity'],
      fruit_months: ETLHelper.extract_array(growth['fruit_months']),
      growth_months: ETLHelper.extract_array(growth['growth_months']),
      maximum_precipitation: ETLHelper.extract_sub(growth['maximum_precipitation'], key: 'mm'),
      minimum_precipitation: ETLHelper.extract_sub(growth['minimum_precipitation'], key: 'mm'),
      maximum_temperature: ETLHelper.extract_sub(growth['maximum_tempurature'], key: 'deg_c'),
      minimum_temperature: ETLHelper.extract_sub(growth['minimum_tempurature'], key: 'deg_c'),
      prefered_soil_nutrients: ETLHelper.descale(growth['soil_nutrients'], factor: 10.0),
      prefered_sand_vs_clay_silt: ETLHelper.descale(growth['soil_nutrients'], factor: 10.0),
      maximum_soil_salinity: ETLHelper.descale(growth['soil_salinity'], factor: 10.0),
      prefered_soil_humidity: ETLHelper.descale(growth['soil_humidity'], factor: 10.0)
    }
  end

  def counterpart(_item)
    slug = plant_item['slug']
    logger.debug "checking #{slug}"
    Plant.find_by(slug: slug)
  end

  def full_item(_item)
    api_id = plant_item['id']
    @api_context.api_request("species/#{api_id}")
  end

  def sync(plant_item)
    db_plant_record = counterpart(plant_item)
    return if stale? db_plant_record

    full_item = full_item(plant_item)
    return if reject? full_item['data']

    logger.debug "updating #{slug}"

    munged_item = munge(full_item)

    etl_helper = ETLHelper(
      etl_version: @etl_version,
      internal: db_plant_record,
      external: munged_item
    )
    etl_helper.sync
  end

  def stale?(item)
    item.nil? || item.etl_meta.etl_version < @etl_version || item.etl_meta.last_runtime < @refresh_cutoff
  end

  def reject?(item)
    growth = item['growth']
    specs = item['specifications']
    result = specs.nil? ||
             specs['ligneous_type'] == 'tree' ||
             item['common_name'].nil? ||
             growth.nil? ||
             growth['average_height'].nil? && growth['row_spacing'].nil?
    logger.debug "rejecting #{item['slug']}" if result
    result
  end

  def edible_plants
    @api_context.paged_api_request('species', { filter_not: { edible_part: nil } }) # get all edible non-tree plants
  end
end
class ETLHelper
  def init(etl_version:, internal:, external:)
    @etl_version = etl_version
    @internal = internal
    @external = external
  end

  def add_meta(item)
    # logger.debug "api data: #{from}"
    item = Plant.new if item.nil?
    # save off metadata
    meta = item.etl_meta
    meta = EtlMeta.new(etl_record: to) if meta.nil?
    meta.last_runtime = DateTime.now
    meta.etl_version = @etl_version
    meta.save!
  end

  def self.extract_sub(value, key:)
    value.nil? ? nil : value[key]
  end

  def self.extract_array(value)
    return [] if value.nil?

    value.map { |item| item.downcase.to_sym }
  end

  def self.descale(value, factor:)
    return nil if value.nil?

    value / factor
  end

  def sync
    logger.debug "current record: #{@internal}"
    # save off plant info
    @external.each do |key, value|
      next if value.nil?

      @internal[key] = value
    end
    # write @internal db
    @internal.save!
    add_meta
  end
end
class PlantApiContext
  def init
    @api_base_url = 'https://trefle.io/api/v1'
    @api_token = Rails.application.credentials[:api_token][:trefle]
  end

  def paged_api_request(params = {})
    head = api_request(endpoint, params)
    results_count = head['meta']['total']
    page_size = head['data'].count
    page_count = Integer(results_count / page_size) + 1
    logger.debug 'feching  pages of metadata'
    (1..page_count).map do |page_num|
      params[:page] = page_num
      page = api_request(endpoint, params)
      page['data']
    end
  end

  def api_request(endpoint, params = {})
    params[:token] = @api_token
    url = "#{@api_base_url}/#{endpoint}"
    response = RestClient.get url, { accept: :json, params: params }
    JSON.parse(response)
  end
end
